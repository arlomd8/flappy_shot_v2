package com.example.flappyshotv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import java.util.Random;

public class EnemyBird {

    private Context context;

    private RectF rect;
    private Random random;

    private float length;
    private float height;
    public float actorXPosition;
    public float actorYPosition;

    public float startActorPos;

    private float x;
    private float y;

    private float m;
    private float n;

    private float paddleSpeed;

    Bitmap bitmapActor;
    boolean isMoving = true;
    int frameWidth = 200;
    int frameHeight = 200;
    int frameCount = 3;
    int currentFrame = 0;
    long lastFrameChangeTime = 0;
    int frameLengthInMilliseconds = 100;
    Rect frameToDraw = new Rect(0, 0 ,frameWidth,frameHeight);
    RectF whereToDraw;

    EnemyBird(Context current, int screenX, int screenY)
    {
        random = new Random();
        this.context = current;
        length = 130;
        height = 20;

        x = 2 * screenX - length;
        y = screenY - 100;
        actorXPosition = screenX;
        actorYPosition = random.nextInt((int) y - (int) height);

        startActorPos = actorXPosition;

        m = screenX;
        n = screenY;

        paddleSpeed = random.nextInt(550);


        bitmapActor = BitmapFactory.decodeResource(context.getResources(),R.drawable.enem);
        bitmapActor = Bitmap.createScaledBitmap(bitmapActor,frameWidth*frameCount,frameHeight, false);
        whereToDraw = new RectF(actorXPosition,actorYPosition, actorXPosition + frameWidth, actorYPosition + frameHeight);
    }

    public void getCurrentFrame()
    {
        long time = System.currentTimeMillis();
        if(isMoving)
        {
            if ( time > lastFrameChangeTime + frameLengthInMilliseconds)
            {
                lastFrameChangeTime = time;
                currentFrame++;
                if (currentFrame >= frameCount)
                {
                    currentFrame = 0;
                }
            }
        }
        frameToDraw.left = currentFrame * frameWidth;
        frameToDraw.right = frameToDraw.left + frameWidth;
    }

    public void update(long fps)
    {
        actorXPosition = actorXPosition - paddleSpeed / fps;

        if (actorXPosition <= 0 - length/2 ) {
            actorXPosition = startActorPos;
            actorYPosition = random.nextInt((int) n - (int) length);
        }
    }

    public void reset(long fps)
    {
        actorXPosition = actorXPosition - paddleSpeed / fps;
        actorXPosition = startActorPos;
        actorYPosition = random.nextInt((int) n - (int) length);

    }

    RectF getCollisionShape () {
        return new RectF(actorXPosition, actorYPosition, actorXPosition + length, actorYPosition + frameHeight / 2);
    }
}
