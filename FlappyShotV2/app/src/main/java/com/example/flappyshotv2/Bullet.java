package com.example.flappyshotv2;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.Random;

import static com.example.flappyshotv2.MainActivity.screenRatioX;
import static com.example.flappyshotv2.MainActivity.screenRatioY;

public class Bullet {
    private Context context;
    private Random random;

    private float length;
    private float height;
    public float actorXPosition;
    public float actorYPosition;

    public float startActorPos;

    private float x;
    private float y;

    private float m;
    private float n;

    private float paddleSpeed;

    Bitmap bitmapActor;
    int frameWidth = 100;
    int frameHeight = 100;
    Rect frameToDraw = new Rect(0, 0 ,frameWidth,frameHeight);

    Bullet(Context current, int screenX, int screenY, float playerPos)
    {
        random = new Random();
        this.context = current;
        length = 50;
        height = 50;

        x = 2 * screenX - length;
        y = screenY - 100;
        actorXPosition = length * 2;
        actorYPosition = playerPos;

        startActorPos = actorXPosition;

        m = screenX;
        n = screenY;

        paddleSpeed = random.nextInt(75);

        bitmapActor = BitmapFactory.decodeResource(context.getResources(),R.drawable.shoot1);
        bitmapActor = Bitmap.createScaledBitmap(bitmapActor, (int)length, (int) height, false);
    }

    public void update(long fps, float playerPos)
    {
        actorXPosition = actorXPosition + paddleSpeed;

        if (actorXPosition >= m - length/2 )
        {
            actorXPosition = startActorPos;
            actorYPosition = playerPos;
        }

    }

    RectF getCollisionShape () {
        return new RectF((int) actorXPosition,(int) actorYPosition, (int)actorXPosition + (int) length, (int) actorYPosition + height * 2);
    }

    public void reset(float playerPos)
    {
        actorXPosition = startActorPos;
        actorYPosition = playerPos;
    }
}