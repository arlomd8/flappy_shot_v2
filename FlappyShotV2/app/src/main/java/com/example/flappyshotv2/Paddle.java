package com.example.flappyshotv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;


public class Paddle
{
    private Context context;

    public static float length;
    public static float height;
    public static float actorXPosition;
    public static float actorYPosition;

    public float x;
    public float y;

    private float m;
    private float n;

    private float paddleSpeed;

    public final int STOPPED = 0;
    public final int DOWN = 1;
    public final int UP = 2;


    private int paddleMoving = STOPPED;

    Bitmap bitmapActor, bitmapActor2;
    boolean isMoving = true;
    static int frameWidth = 200;
    static int frameHeight = 200;
    int frameCount = 6;
    int currentFrame = 0;
    long lastFrameChangeTime = 0;
    int frameLengthInMilliseconds = 100;
    Rect frameToDraw = new Rect(0, 0 ,frameWidth,frameHeight);
    RectF whereToDraw;


    Paddle(Context current, int screenX, int screenY)
    {
        this.context = current;
        length = 130;
        height = 20;

        x = 0;
        y = screenY / 2;
        actorXPosition = 0;
        actorYPosition = y - frameHeight/2;



        m = screenX;
        n = screenY;

        paddleSpeed = 750;

        bitmapActor = BitmapFactory.decodeResource(context.getResources(),R.drawable.birb);
        bitmapActor2 = BitmapFactory.decodeResource(context.getResources(),R.drawable.birb);

        bitmapActor = Bitmap.createScaledBitmap(bitmapActor,frameWidth*frameCount,frameHeight, false);
        bitmapActor2 = Bitmap.createScaledBitmap(bitmapActor,frameWidth*frameCount,frameHeight, false);
        whereToDraw = new RectF(actorXPosition,actorYPosition, actorXPosition + frameWidth, actorYPosition + frameHeight);



    }

    public void getCurrentFrame()
    {
        long time = System.currentTimeMillis();
        if(isMoving)
        {
            if ( time > lastFrameChangeTime + frameLengthInMilliseconds)
            {
                lastFrameChangeTime = time;
                currentFrame++;
                if (currentFrame >= frameCount)
                {
                    currentFrame = 0;
                }
            }
        } 
        frameToDraw.left = currentFrame * frameWidth;
        frameToDraw.right = frameToDraw.left + frameWidth;
    }

    public void setMovementState(int state)
    {
        paddleMoving = state;
    }

    public static RectF getCollisionShape () {
        return new RectF(actorXPosition,actorYPosition, actorXPosition + length, actorYPosition + height);
    }

    public int getPaddlePos()
    {
        return (int) actorYPosition;
    }

    public void update(long fps)
    {
        if (actorYPosition > 0)
        {
            if (paddleMoving == DOWN)
            {
                actorYPosition = actorYPosition - paddleSpeed / fps;
            }
        }

        if (actorYPosition + frameHeight < n )
        {
            if (paddleMoving == UP)
            {
                actorYPosition = actorYPosition + paddleSpeed / fps;
            }
        }
    }


    //RESET PADDLE TO POSITION

    public void reset (int p, int q)
    {
        //RESET POSISI X/Y PADDLE
        x = 0;
        y = q / 2;
        actorXPosition = 0;
        actorYPosition = y - frameHeight/2;
    }


}