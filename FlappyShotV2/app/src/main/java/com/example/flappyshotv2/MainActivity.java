package com.example.flappyshotv2;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Debug;
import android.os.Looper;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.AdRequest;



public class MainActivity extends Activity
{
    private GameView parallaxView;
    public static float screenRatioX, screenRatioY;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point resolution = new Point();
        display.getSize(resolution);
        parallaxView = new GameView(this, resolution.x, resolution.y);
        setContentView(parallaxView);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }


            @Override
            public void onAdClosed() {
                parallaxView.ResetBird();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                // Code to be executed when the interstitial ad is closed.
            }
        });

    }

    public class GameView extends SurfaceView implements Runnable
    {
        ArrayList<Background> backgrounds;
        volatile boolean running;
        boolean paused = true;
        Thread gameThread = null;
        Paint paint;
        Canvas canvas;
        SurfaceHolder ourHolder;
        Context context;
        long fps = 60;
        int screenWidth;
        int screenHeight;

        volatile boolean playing;
        boolean gameOver = false;
        private long timeThisFrame;

        int screenX;
        int screenY;
        Paddle paddle;

        EnemyBird[] enemyBird;
        Bullet[] bullets;
        int lives = 3;
        int score = 0;
        int highScore = 0;

        // For sound FX
        SoundPool soundPool;
        int eggCollID = -1;
        int eggShootID = -1;
        int gameOverID = -1;
        int loseLifeID = -1;
        int bgmID = -1;


        GameView(Context context, int screenWidth, int screenHeight)
        {
            super(context);
            this.context= context;
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;
            ourHolder = getHolder();
            paint = new Paint();
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenX = size.x;
            screenY = size.y;

            screenRatioX = 1920f / screenWidth;
            screenRatioY = 1080f / screenHeight;


            backgrounds = new ArrayList <>();
            backgrounds.add(new Background(this.context, screenWidth, screenHeight, "bg",  -10, 100, 50));

            paddle = new Paddle (context, screenX, screenY);

            enemyBird = new EnemyBird[4];

            for (int i = 0; i < 4; i++ )
            {
                EnemyBird bird = new EnemyBird(context, screenX, screenY);
                enemyBird[i] = bird;
            }

            bullets = new Bullet[4];

            for (int i = 0; i < 4; i++)
            {
                Bullet bullet = new Bullet(context, screenX, screenY, paddle.getPaddlePos() + 75);
                bullets[i] = bullet;
            }

            soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC,0);
            try{
                // Create objects of the 2 required classes
                AssetManager assetManager = context.getAssets();
                AssetFileDescriptor descriptor;

                // Load our fx in memory ready for use
                descriptor = assetManager.openFd("eggcoll.ogg");
                eggCollID = soundPool.load(descriptor, 0);


                descriptor = assetManager.openFd("eggshoot.ogg");
                eggShootID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("gameOver.ogg");
                gameOverID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("looseLife.ogg");
                loseLifeID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("bgm.ogg");
                bgmID = soundPool.load(descriptor, 0);

            }
            catch(IOException e)
            {
                // Print an error message to the console
                Log.e("error", "failed to load sound files");
            }


        }



        public void ResetBird()
        {
            if (lives == 0) {
                if(highScore < score)
                {
                    highScore = score;
                }

                score = 0;
                lives = 3;
                gameOver = false;
                playing = true;
                paused = false;
            }

            paddle.reset(screenX, screenY);
            for(EnemyBird bird: enemyBird) {
                bird.update((fps));
                bird.reset(fps);
            }
            for (Bullet bullet : bullets)
            {
                bullet.reset(paddle.getPaddlePos()+75);
            }

        }

        private void update()
        {

            paddle.update(fps);

            for (Background bg : backgrounds)
            {
                bg.update(fps);

            }

            for(EnemyBird bird: enemyBird)
            {
                bird.update(fps);

                if (RectF.intersects(bird.getCollisionShape(), Paddle.getCollisionShape()))
                {
                    soundPool.play(loseLifeID, 1, 1, 0, 0, 1);

                    bird.reset(fps);
                    lives--;
                    if(lives == 0)
                    {
                        gameOver = true;
                        paused = true;
                        playing = false;
                        soundPool.play(gameOverID, 1, 1, 0, 0, 1);
                        draw();
                    }
                    return;
                }

                for (Bullet bullet : bullets)
                {
                    if (RectF.intersects(bird.getCollisionShape(), bullet.getCollisionShape()))
                    {
                        soundPool.play(eggCollID, 1, 1, 0, 0, 1);
                        bird.reset(fps);
                        bullet.reset(paddle.getPaddlePos()+75);
                        score += 1;
                        return;
                    }
                }




            }

            for (Bullet bullet : bullets)
            {
                bullet.update(fps, paddle.getPaddlePos()+75);
            }
        }

        public void showInterstitial() {
            if(Looper.myLooper() != Looper.getMainLooper()) {
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        doShowInterstitial();
                    }
                });
            } else {
                doShowInterstitial();
            }
        }
        private void doShowInterstitial() {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
            }
        }
        private void draw()
        {
            if (ourHolder.getSurface().isValid())
            {
                canvas = ourHolder.lockCanvas();
                canvas.drawColor(Color.argb(255, 0, 3, 70));
                canvas.drawColor(Color.argb(255, 26, 128, 182));


                drawBackground(0);

                paddle.whereToDraw.set((int)paddle.actorXPosition, (int)paddle.actorYPosition, (int)paddle.actorXPosition + paddle.frameWidth, paddle.actorYPosition + paddle.frameHeight);
                paddle.getCurrentFrame();

                canvas.drawBitmap(paddle.bitmapActor, paddle.frameToDraw, paddle.whereToDraw, paint);

                for (EnemyBird bird : enemyBird)
                {
                    bird.whereToDraw.set((int)bird.actorXPosition, (int)bird.actorYPosition, (int)bird.actorXPosition + bird.frameWidth, bird.actorYPosition + bird.frameHeight);
                    bird.getCurrentFrame();

                    canvas.drawBitmap(bird.bitmapActor, bird.frameToDraw, bird.whereToDraw, paint);
                }

                for (Bullet bullet : bullets)
                {
                    canvas.drawBitmap(bullet.bitmapActor, bullet.actorXPosition, bullet.actorYPosition, paint);
                }


                paint.setTextSize(60);
                paint.setColor(Color.argb(255, 255, 255, 0));
                canvas.drawText("Score: " + score + "   Lives: " + lives, 10,50, paint);

                paint.setTextSize(60);
                paint.setColor(Color.argb(255, 255, 255, 0));
                canvas.drawText("HighScore: " + highScore , screenX - 540,50, paint);

                if(gameOver)
                {
                    paint.setTextSize(90);
                    canvas.drawText("GAME OVER", 10, screenY/2, paint);
                    canvas.drawText("YOUR SCORE IS: " + score, 10,screenY/2 + 90, paint);
                    soundPool.play(gameOverID, 1, 1, 0, 0, 1);

                    try {
                        gameThread.sleep(2000);
                        showInterstitial();

                    } catch (Exception e) {
                        Log.e("Error:", "joining thread");

                    }
                }

                paint.setColor(Color.argb(255,255,255,0));
                paint.setColor(Color.argb(255, 255, 255, 0));
                paint.setTextSize(60);
                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        private void drawBackground(int position) {

            Background bg = backgrounds.get(position);
            Rect fromRect1 = new Rect(0, 0, bg.width - bg.xClip, bg.height);
            Rect toRect1 = new Rect(bg.xClip, bg.startY, bg.width, bg.endY);

            Rect fromRect2 = new Rect(bg.width - bg.xClip, 0, bg.width, bg.height);
            Rect toRect2 = new Rect(0, bg.startY, bg.xClip, bg.endY);

            if (!bg.reversedFirst)
            {
                canvas.drawBitmap(bg.bitmap, fromRect1, toRect1, paint);
                canvas.drawBitmap(bg.bitmapReversed, fromRect2, toRect2, paint);
            }
            else
            {
                canvas.drawBitmap(bg.bitmap, fromRect2, toRect2, paint);
                canvas.drawBitmap(bg.bitmapReversed, fromRect1, toRect1, paint);
            }

        }

        @Override
        public void run()
        {
            while (running)
            {
                long startFrameTime = System.currentTimeMillis();

                update();

                draw();

                long timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame >= 1)
                {
                    fps = 1000 / timeThisFrame;
                }
            }
            while(playing)
            {
                long startFrameTime = System.currentTimeMillis();

                if(!paused)
                {
                    update();
                }

                draw();
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if(timeThisFrame > 0)
                {
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent)
        {
            switch(motionEvent.getAction() & MotionEvent.ACTION_MASK)
            {
                case MotionEvent.ACTION_DOWN:
                    paused = false;


                    if(motionEvent.getY() > screenY / 2)
                    {
                        paddle.setMovementState(paddle.UP);
                    }

                    else
                    {
                        paddle.setMovementState(paddle.DOWN);
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    paddle.setMovementState((paddle.STOPPED));
                    break;
            }
            return true;
        }

        public void pause()
        {
            running = false;
            try
            {
                gameThread.join();
            }
            catch (InterruptedException e)
            {
                Log.e("Error: ", "Joining thread");
            }
            playing = false;
        }

        public void resume()
        {
            running = true;
            gameThread = new Thread(this);
            gameThread.start();
            playing = true;
        }
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        parallaxView.resume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        parallaxView.pause();
    }
}

